package com.example.mysecondassignmentwithdatabindingandviewmodalandlivedata.viewModal

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

// This is my viewModal class and this extends superclass ViewModal
class ViewModalemoActivityViewModal :ViewModel(){
    // Here i Use Mutable Livedata Class it means it can be change over and over with encapsulation concept any user can't access directly count variable
    private var _count:MutableLiveData<Int> = MutableLiveData()

    // this is helper variable which contains latest update on count variable and I will use it every time
    val totleCount: LiveData<Int> =_count


    // this is init block to initialize count variable with default value
    init {
        _count.value=125
    }

   // this is function which will be call then you hit button
    fun updateCount(input:Int){
       _count.value= _count.value?.plus(input)
    }


}